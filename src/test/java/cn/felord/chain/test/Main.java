package cn.felord.chain.test;

import cn.felord.chain.one.*;

/**
 * @author Dax
 * @since 17:10  2019/9/24
 */
public class Main {

    public static void main(String[] args) {

        twoModeTest();

    }


    private static void oneModeTest() {
        DefaultHandlerChain chain = new DefaultHandlerChain();

        chain.addHandler(new HeaderHandler());
        chain.addHandler(new BodyHandler());
        chain.addHandler(new FooterHandler());

        RequestSource requestSource = new RequestSource();

        requestSource.setHeader(1001);
        requestSource.setBody(1002);
        requestSource.setFooter(1003);

        chain.doChain(requestSource);

    }

    private static void twoModeTest() {


        RequestSource requestSource = new RequestSource();

        requestSource.setHeader(1001);
        requestSource.setBody(1002);
        requestSource.setFooter(1003);
        cn.felord.chain.two.FooterHandler footerHandler = new cn.felord.chain.two.FooterHandler(null);
        cn.felord.chain.two.BodyHandler bodyHandler = new cn.felord.chain.two.BodyHandler(footerHandler);
        cn.felord.chain.two.HeaderHandler headerHandler = new cn.felord.chain.two.HeaderHandler(bodyHandler);


        headerHandler.doHandler(requestSource);

    }


}
