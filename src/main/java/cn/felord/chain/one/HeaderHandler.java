package cn.felord.chain.one;

/**
 * @author Dax
 * @since 16:02  2019/9/24
 */
public class HeaderHandler implements Handler {
    @Override
    public void doHandler(RequestSource requestSource, HandlerChain handlerChain) {
        Integer header = requestSource.getHeader();
        System.out.println("header handler= " + header);

        handlerChain.doChain(requestSource);
    }
}
