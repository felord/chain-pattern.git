package cn.felord.chain.one;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dax
 * @since 15:39  2019/9/24
 */
public class DefaultHandlerChain implements HandlerChain {

    private int pos = 0;
    private List<Handler> handlers = new ArrayList<>();


    public void addHandler(Handler handler) {
        handlers.add(handler);
    }

    @Override
    public void doChain(RequestSource requestSource) {
        int size = handlers.size();
        if (pos < size) {
            Handler handler = handlers.get(pos++);
            handler.doHandler(requestSource, this);
        }
    }
}
