package cn.felord.chain.one;

/**
 * The interface Handler chain.
 *
 * @author Dax
 * @since 15 :30  2019/9/24
 */
public interface HandlerChain {
    /**
     * 调用handler 处理 source.
     *
     * @param requestSource the request source
     */
    void doChain(RequestSource requestSource);
}
