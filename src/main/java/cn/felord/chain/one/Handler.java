package cn.felord.chain.one;

/**
 * The interface Handler.
 *
 * @author Dax
 * @since 15 :40  2019/9/24
 */
public interface Handler {
    /**
     * Do handler.
     *
     * @param requestSource the request source
     * @param handlerChain  the handler chain
     */
    void doHandler(RequestSource requestSource, HandlerChain handlerChain);
}
