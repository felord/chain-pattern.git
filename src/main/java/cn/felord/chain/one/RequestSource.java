package cn.felord.chain.one;

/**
 * @author Dax
 * @since 15:31  2019/9/24
 */
public class RequestSource  {

    private Integer header;
    private Integer body;
    private Integer footer;

    public Integer getHeader() {
        return header;
    }

    public void setHeader(Integer header) {
        this.header = header;
    }

    public Integer getBody() {
        return body;
    }

    public void setBody(Integer body) {
        this.body = body;
    }

    public Integer getFooter() {
        return footer;
    }

    public void setFooter(Integer footer) {
        this.footer = footer;
    }
}
