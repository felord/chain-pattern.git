package cn.felord.chain.one;

/**
 * @author Dax
 * @since 16:05  2019/9/24
 */
public class BodyHandler implements Handler {
    @Override
    public void doHandler(RequestSource requestSource, HandlerChain handlerChain) {
        Integer body = requestSource.getBody();
        System.out.println("body = " + body);
        if (body > requestSource.getFooter()) {
            handlerChain.doChain(requestSource);
        }

    }
}
