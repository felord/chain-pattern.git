package cn.felord.chain.one;

/**
 * @author Dax
 * @since 16:04  2019/9/24
 */
public class FooterHandler implements Handler {
    @Override
    public void doHandler(RequestSource requestSource, HandlerChain handlerChain) {
        Integer footer = requestSource.getFooter();
        System.out.println("footer = " + footer);

        handlerChain.doChain(requestSource);
    }
}
