package cn.felord.chain.two;

import cn.felord.chain.one.RequestSource;

/**
 * @author Dax
 * @since 17:34  2019/9/24
 */
public class FooterHandler implements Handler {
    private Handler next;

    public FooterHandler(Handler next) {
        this.next = next;
    }

    @Override
    public Handler getNext() {
        return next;
    }

    @Override
    public void doHandler(RequestSource requestSource) {
        Integer footer = requestSource.getFooter();
        System.out.println("footer = " + footer);
        if (next != null) {
            next.doHandler(requestSource);
        }
    }
}
