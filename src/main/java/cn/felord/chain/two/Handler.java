package cn.felord.chain.two;

import cn.felord.chain.one.RequestSource;

/**
 * The interface Handler.
 *
 * @author Dax
 * @since 17 :20  2019/9/24
 */
public interface Handler {
    /**
     * 指针指向下一个处理节点.
     *
     * @return the next
     */
    Handler getNext();

    /**
     * 处理具体逻辑.
     *
     * @param requestSource the request source
     */
    void doHandler(RequestSource requestSource);
}
