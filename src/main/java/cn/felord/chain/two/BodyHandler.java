package cn.felord.chain.two;

import cn.felord.chain.one.RequestSource;

/**
 * @author Dax
 * @since 17:33  2019/9/24
 */
public class BodyHandler implements Handler {
    private Handler next;

    public BodyHandler(Handler next) {
        this.next = next;
    }

    @Override
    public Handler getNext() {
        return next;
    }

    @Override
    public void doHandler(RequestSource requestSource) {
        Integer body = requestSource.getBody();
        System.out.println("body = " + body);
        if (next != null) {
            next.doHandler(requestSource);
        }
    }
}
