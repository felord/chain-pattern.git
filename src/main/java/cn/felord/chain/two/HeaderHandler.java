package cn.felord.chain.two;

import cn.felord.chain.one.RequestSource;

/**
 * @author Dax
 * @since 17:22  2019/9/24
 */
public class HeaderHandler implements Handler {
    private Handler next;

    public HeaderHandler(Handler next) {
        this.next = next;
    }

    @Override
    public Handler getNext() {
        return next;
    }

    @Override
    public void doHandler(RequestSource requestSource) {
        Integer header = requestSource.getHeader();
        System.out.println("header = " + header);
        if (next != null) {
            next.doHandler(requestSource);
        }
    }
}
